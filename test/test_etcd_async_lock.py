import asyncio
import pytest

from thlock import EtcdLock


@pytest.mark.asyncio
async def test_lock():
    '''
    Test etcd lock functions: acquire, release, close - success expected
    '''
    HOST = 'etcd-test'
    PORT = 2379

    # lock
    lock = EtcdLock(host=HOST, port=PORT, name='lock-0')
    await lock.acquire()
    await asyncio.sleep(1.0)
    await lock.release()
    await lock.close()


@pytest.mark.asyncio
async def test_lock_is_acquired():
    '''
    Test etcd lock functions: acquire, is_acquired, release, close - success expected
    '''
    HOST = 'etcd-test'
    PORT = 2379

    # lock
    lock = EtcdLock(host=HOST, port=PORT, name='lock-0')
    r = await lock.acquire()
    assert r
    acquired = await lock.is_acquired()
    assert acquired
    await lock.release()
    await lock.close()


@pytest.mark.asyncio
async def test_lock_release():
    '''
    Test etcd lock functions: acquire, release, close - success expected
    '''
    HOST = 'etcd-test'
    PORT = 2379

    # lock
    lock = EtcdLock(host=HOST, port=PORT, name='lock-0')
    await lock.acquire()
    r = await lock.release()
    assert r
    await lock.close()


@pytest.mark.asyncio
async def test_lock_acquire_timeout():
    '''
    Test etcd lock functions: acquire with timeout, release, close - success expected
    '''
    HOST = 'etcd-test'
    PORT = 2379

    # lock
    lock = EtcdLock(host=HOST, port=PORT, name='lock-0')
    await lock.acquire(timeout=10)
    r = await lock.release()
    assert r
    await lock.close()
